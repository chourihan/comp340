calc :: String -> [Float]
calc = foldl f [] . words
f (x:y:zs) "add" = y+x:zs
f (x:y:zs) "subtract" = y-x:zs
f (x:y:zs) "multiply" = y*x:zs
f (x:y:zs) "divide" = y/x:zs
f xs y = read y : xs