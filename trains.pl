directTrain(avon,  braintree).
directTrain(quincy,avon).
directTrain(newton,boston).
directTrain(boston,avon).
directTrain(braintree,milton).
directTrain(westwood,newton).
directTrain(canton,  westwood).
travelBetween(X,Y):- directTrain(X,Y);directTrain(Y,X).
travelBetween(X,Y):- travelBetween(X,Z);travelBetween(W,Y).